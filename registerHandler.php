<p><a href="register.html">Return to registration.</a></p>
<p><a href="index.html">Return to main menu.</a></p>

<?php
// Project Name: Milestone2
// Project Version: 1.1
// Module Name: Login Module
// Module Version: 1.1
// Programmer Name: Justin Gewecke
// Date: 7/5/2020
// Description: This module handles the interaction with the database for a new registering user
// References: https://www.w3schools.com/php/php_mysql_insert.asp

/* ---User Authentification---
 * Username and password <= 50 characters
 * Unlimited attempts allowed
 * Certain characters are not allowed: ' " / \ [ ] ( ) { }
 * Password is hidden when typed
 * Username is case-insensitive
 * Password is case-sensitive !--This is done within phpMyAdmin by settings the PASSWORD column collation to latin1_general_cs--!
 * Password must be bigger than 5 characters
 * Username must be bigger than 5 characters
 */

// Connect
$link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

// Input
$firstname = $_POST['FirstName'];
$lastname = $_POST['LastName'];
$email = $_POST['Email'];
$email2 = $_POST['ReenterEmail'];
$username = $_POST['Username'];
$password = $_POST['Password'];

// Check for empty input, otherwise exit out with an error
if ($firstname == NULL) { echo "The First Name or is a required field and cannot be blank.\n";      return; }
if ($lastname == NULL)  { echo "The Last Name is a required field and cannot be blank.\n";          return; }
if ($email == NULL)     { echo "The email is a required field and cannot be blank.\n";              return; }
if ($email2 == NULL)    { echo "The email re-entry is a required field and cannot be blank.\n";     return; }
if ($username == NULL)  { echo "The username is a required field and cannot be blank.\n";           return; }
if ($password == NULL)  { echo "The password is a required field and cannot be blank.\n";           return; }

// Check length
if (strlen($username) <= 5) {
    die ("Username must have at least 6 characters.");
}

if (strlen($password) <= 5) {
    die ("Password must have at least 6 characters");
}

// Check for illegal characters
$illegal = array("'", "\"", "/", "\\", "[", "]", "{", "}", "(", ")");
foreach ($illegal as $i) {
    if (strpos($username, $i) !== false) {
        echo $i;
        die ("ERROR: The username has one of the illegal characters: ' \" / \ [ ] ( ) { }\n");
    }
}

foreach ($illegal as $i) {
    if (strpos($password, $i) !== false) {
        die ("ERROR: The password has one of the illegal characters: ' \" / \ [ ] ( ) { }\n");
    }
}






// Sanitizing input to deal with SQL injection
$firstname= str_replace("'", "", $firstname);
$lastname= str_replace("'", "", $lastname);
$email= str_replace("'", "", $email);
$email2= str_replace("'", "", $email2);
$username = str_replace("'", "", $username);
$password= str_replace("'", "", $password);


if ($email != $email2) {
    die("Email does not match! Please try again.");
}

// Attempt insert
$sql = "INSERT INTO users (FIRST_NAME, LAST_NAME, EMAIL, USERNAME, PASSWORD) VALUES ('$firstname', '$lastname', '$email', '$username', '$password')";
if(mysqli_query($link, $sql)){
    echo "Records inserted successfully.";
} else{
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}

// Close connection
mysqli_close($link);
?>