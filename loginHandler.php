<p><a href="login.html">Click here to go back.</a></p>

<?php
// Project Name: Milestone2
// Project Version: 1.1
// Module Name: Login Module
// Module Version: 1.1
// Programmer Name: Justin Gewecke
// Date: 7/5/2020
// Description: This module handles the login operations of the database and the website
// References: https://www.w3schools.com/php/php_mysql_insert.asp

/* ---User Authentification---
 * Username and password <= 50 characters
 * Unlimited attempts allowed
 * Certain characters are not allowed: ' " / \ [ ] ( ) { }
 * Password is hidden when typed
 * Username is case-insensitive
 * Password is case-sensitive !--This is done within phpMyAdmin by settings the PASSWORD column collation to latin1_general_cs--!
 */

// Connect
$link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

// Input
$username = $_POST['Username'];
$password = $_POST['Password'];

// Check for empty input, otherwise exit out with an error
if ($username == NULL)  { echo "The username is a required field and cannot be blank.\n";           return; }
if ($password == NULL)  { echo "The password is a required field and cannot be blank.\n";           return; }

// Sanitizing input in case of sql injection
$username = str_replace("'", "", $username);
$password= str_replace("'", "", $password);

$sql = "SELECT USERNAME, PASSWORD FROM users WHERE USERNAME='$username' AND PASSWORD='$password'";
$result = mysqli_query($link, $sql);
$numRows = mysqli_num_rows($result);

// Check for matches in our database
// We have too many of this user
if ($numRows > 2) {
    echo "There are multiple users registered with that information";
}
// Login Success
else if ($numRows == 1) {
    echo "Login was successful. ";
}
// Login failed
else if ($numRows == 0) {
    echo "Login failed. ";
}
// Login error
else {
    echo "ERROR: Could not able to execute $sql. " . mysqli_error($link);
}

// Close connection
mysqli_close($link);
?>