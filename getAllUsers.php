<?php
// Project Name: Milestone2
// Project Version: 1.1
// Module Name: Login Module
// Module Version: 1.1
// Programmer Name: Justin Gewecke
// Date: 7/5/2020
// Description: This gets all the users in the database (First and last name)
// References: https://www.w3schools.com/php/php_mysql_insert.asp

// Connect
$link = mysqli_connect("127.0.0.1", "azure", "6#vWHD_$", "localdb", "52757");

// Check connection
if($link === false){
    die("ERROR: Could not connect. " . mysqli_connect_error());
}

$sql = "SELECT FIRST_NAME, LAST_NAME FROM users";
$result = mysqli_query($link, $sql);

if (mysqli_num_rows($result) > 0) {
    while ($row = mysqli_fetch_assoc($result)) {
        echo $row["FIRST_NAME"]. " " . $row[LAST_NAME]. "<br>";
    }
}
else {
    echo "0 results";
}


// Close connection
mysqli_close($link);
?>